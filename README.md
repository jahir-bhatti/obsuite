# Openbox Suite
- Collection of system indicators and utilities for custom desktop environments

# System monitoring
- Battery level
- Temperature
- Network status
- D-Bus signals

**Extra features**
- Backlight and keyboard backlight control
- Multiple batteries support
- Bluetooth support, automated connection routine for selected devices
- Customizable notifications
- Customizable tray icons and d-bus actions
- Customs actions for battery and temperature critical levels
- Smart selection of temperature probe when available
- Notification daemon API (--popup)
- Systemd inhibit wrapper

![alt tag](https://gitlab.com/william.belanger/obsuite/raw/master/screenshots/desktop.png)

![alt tag](https://gitlab.com/william.belanger/obsuite/raw/master/screenshots/tray.png)

![alt tag](https://gitlab.com/william.belanger/obsuite/raw/master/screenshots/notifications.png)

# Launcher
- Keyboard driven, improved gmrun replacement
- History based suggestions
- Dynamic scan of desktop files, blacklist
- Command draft system
- Command validation
- Exit on focus out
- Compatible with ~ and bash alias

![alt tag](https://gitlab.com/william.belanger/obsuite/raw/master/screenshots/launcher.gif)

# Splash screen
- Improved oblogout replacement
- Customizable icons and commands
- Customizable background and font effects
- Conky-like stdout fetching

![alt tag](https://gitlab.com/william.belanger/obsuite/raw/master/screenshots/splash.gif)

# Preferences

![alt tag](https://gitlab.com/william.belanger/obsuite/raw/master/screenshots/preferences.gif)

# Command line interface
    --action widget "action name": Call a widget action, as defined in the preferences dialog
    --preferences: Toggle preferences dialog
    --quit: Close all widgets
    --reload: Destroy and restart actives widgets
    --run: Execute launcher widget
    --splash: Execute splash screen
    --popup: Display a custom popup notification
      Available options:
          mode="queue"
          msg=str
          icon=str (path)
          width=int
          height=int
          duration=int
          x_offset=int
          y_offset=int
          font_size=int
          font_family=str
          opacity=float
          foreground=hex
          background=hex
          position="top/middle/bottom left/center/right"

# Installation
- Arch Linux: install 'obsuite-git' from the AUR
