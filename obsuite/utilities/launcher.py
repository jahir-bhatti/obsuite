#!/usr/bin/python3
import json
import os
import re
import shlex
from PyQt5 import QtCore, QtGui, QtWidgets

try:
    from ..__id__ import ID
except (ImportError, ValueError):
    from __id__ import ID

LOCAL_DIR = os.path.dirname(os.path.realpath(__file__)) + '/'
HISTORY_FILE = os.path.expanduser(f"~/.config/{ID}/launcher.json")


class CommandLine(QtWidgets.QLineEdit):
    clearRecent = QtCore.pyqtSignal()
    close = QtCore.pyqtSignal()
    execute = QtCore.pyqtSignal()
    focusList = QtCore.pyqtSignal(int)
    focusDraft = QtCore.pyqtSignal()
    focusOut = QtCore.pyqtSignal()
    hideDraft = QtCore.pyqtSignal()
    saveDraft = QtCore.pyqtSignal(str)
    resizeMain = QtCore.pyqtSignal()
    setViewMode = QtCore.pyqtSignal(str)

    def __init__(self, parent):
        super().__init__()
        self.parent = parent
        self.textEdited.connect(self._textEditedSignal)
        self.textChanged.connect(self._textChangedSignal)
        self.index = 0
        self.setTextMargins(5, 2, 5, 2)
        self.setAutoFillBackground(True)
        self.setFrame(False)
        self.setFocusPolicy(QtCore.Qt.StrongFocus)
        self.setFocus(True)

    def _textChangedSignal(self, text):
        # Reset palette to default (remove red palette)
        self.setPalette(self.parent.palette())
        self.setAttribute(QtCore.Qt.WA_SetPalette, False)

    def _textEditedSignal(self, text):
        self.setViewMode.emit("match")
        self.hideDraft.emit()

    def focusInEvent(self, event):
        self.hideDraft.emit()
        self.resizeMain.emit()
        QtWidgets.QLineEdit.focusInEvent(self, event)

    def focusOutEvent(self, event):
        self.saveDraft.emit(self.text())
        self.focusOut.emit()

    def keyReleaseEvent(self, event):
        if event.key() == QtCore.Qt.Key_Backspace:
            self.backspace = False
        QtWidgets.QLineEdit.keyReleaseEvent(self, event)

    def keyPressEvent(self, event):
        key = event.key()
        draft = self.parent.draftList
        recent = self.parent.recentList

        if key == QtCore.Qt.Key_Escape:
            self.close.emit()

        elif key in (QtCore.Qt.Key_Return, QtCore.Qt.Key_Enter):
            self.execute.emit()

        elif key == QtCore.Qt.Key_Up:
            if recent.isVisible():
                lastRow = recent.count() - 1
                self.focusList.emit(lastRow)
            elif draft.isVisible():
                self.focusDraft.emit()

        elif key == QtCore.Qt.Key_Down:
            if draft.isVisible():
                self.focusDraft.emit()
            elif recent.isVisible():
                self.focusList.emit(0)

        elif key == QtCore.Qt.Key_Backspace and event.modifiers() == QtCore.Qt.ShiftModifier:
            self.clear()
            self.textEdited.emit("")

        self.backspace = key == QtCore.Qt.Key_Backspace
        QtWidgets.QLineEdit.keyPressEvent(self, event)

    def reload(self):
        if self.parent.db.history:
            last = self.parent.db.history[0]
            self.setText(last)
            self.selectAll()


class CommonListItem(QtWidgets.QListWidgetItem):
    def __init__(self, parent, cmd, icon):
        super().__init__()
        self.preferences = parent.preferences

        if icon == "draft":
            qIcon = parent.icons["draft"]

        elif icon in ("recent", "desktop"):
            # Verify if the command correspond to theme icon
            basename = os.path.basename(cmd)
            qIcon = QtGui.QIcon.fromTheme(basename)
            if qIcon.isNull():

                # Verify if the command exist in a desktop file
                if cmd in parent.desktopFiles.cmds:
                    qIcon = parent.desktopFiles.icons[cmd]
                    qIcon = QtGui.QIcon.fromTheme(qIcon)

                    # Fallback for desktop file invalid icon
                    if qIcon.isNull():
                        qIcon = parent.icons["default"]

            # No matching icon found
            if qIcon.isNull():
                qIcon = parent.icons["recent"]

        font = QtGui.QFont()
        fontSize = self.preferences.get("parameters", "launcher", "font size")
        if fontSize:
            font.setPointSize(fontSize)
        if icon == "desktop":
            font.setItalic(True)

        self.setFont(font)
        self.setIcon(qIcon)
        self.setText(cmd)


class CommonList(QtWidgets.QListWidget):
    updateCmd = QtCore.pyqtSignal(str)
    execute = QtCore.pyqtSignal()
    focusCmd = QtCore.pyqtSignal(bool)
    focusOut = QtCore.pyqtSignal()
    resizeMain = QtCore.pyqtSignal()
    suggestRecent = QtCore.pyqtSignal()
    discard = QtCore.pyqtSignal(str)

    def __init__(self, parent):
        super().__init__()
        self.parent = parent
        self.preferences = parent.preferences
        self.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.setFocusPolicy(QtCore.Qt.StrongFocus)
        self._setIconSize()
        self.itemPressed.connect(self._itemPressed)
        self.currentItemChanged.connect(self._selectionChanged)

    def _deleteItemHook(self):
        pass

    def _switchFocusUp(self):
        pass

    def _switchFocusDown(self):
        pass

    def _clearSelection(self):
        for i in range(self.count()):
            self.item(i).setSelected(False)

    def _deleteSelection(self):
        currentRow = self.currentRow()
        self._deleteItemHook()
        if self.parent.view == "recent":
            self.suggestRecent.emit()
            self.setCurrentRow(currentRow)

    def _itemPressed(self, item):
        if self.preferences.get("parameters", "launcher", "exec on click"):
            self.execute.emit()

    def _postKey(self, event):
        try:
            sequence = QtGui.QKeySequence(event.key())
            sequence = sequence.toString().lower()
            sequence = sequence.replace("space", " ")
            sequence.encode('utf-8')
        except UnicodeEncodeError:
            # Filter unwanted characters from modifiers
            sequence = None
        keyEvent = QtGui.QKeyEvent(QtCore.QEvent.KeyPress, event.key(), event.modifiers(), sequence)
        if self.parent:
            QtCore.QCoreApplication.postEvent(self.parent.cmdLine, keyEvent)

    def _selectionChanged(self, current, previous):
        if current:
            selectedText = current.text()
            self.updateCmd.emit(selectedText)

    def _setIconSize(self):
        dummy = QtWidgets.QListWidgetItem()
        fontSize = self.preferences.get("parameters", "launcher", "font size")
        if fontSize:
            font = QtGui.QFont()
            font.setPointSize(fontSize)
            dummy.setFont(font)
        self.addItem(dummy)

        hint = self.sizeHintForRow(0)
        iconSize = QtCore.QSize(hint, hint)
        self.setIconSize(iconSize)
        self.takeItem(0)

    def focusInEvent(self, event):
        # Force rearm of currentItemChanged signal on widget change
        self.setCurrentRow(-1)

    def focusOutEvent(self, event):
        self._clearSelection()
        self.focusOut.emit()

    def keyPressEvent(self, event):
        key = event.key()

        if key in (QtCore.Qt.Key_Return, QtCore.Qt.Key_Enter):
            self.execute.emit()

        elif key == QtCore.Qt.Key_Delete:
            self._deleteSelection()

        elif key == QtCore.Qt.Key_Down:
            lastRow = self.count() - 1
            if self.currentRow() == lastRow:
                self._switchFocusDown()
                event.ignore()

        elif key == QtCore.Qt.Key_Up:
            if self.currentRow() == 0:
                self._switchFocusUp()
                event.ignore()

        else:
            self._postKey(event)
            self.focusCmd.emit(True)

        if event.isAccepted():
            QtWidgets.QListWidget.keyPressEvent(self, event)


class DraftList(CommonList):
    focusList = QtCore.pyqtSignal(int)

    def __init__(self, parent):
        super().__init__(parent)
        font = self.font()
        font.setItalic(True)
        item = CommonListItem(parent, cmd="", icon="draft")
        item.setFont(font)
        self.addItem(item)

        height = self.sizeHintForRow(0)
        self.setFixedHeight(height)
        self.hide()

    def _deleteItemHook(self):
        if self.currentItem():
            currentText = self.currentItem().text()
            self.discard.emit(currentText)
        self.hide()
        self.resizeMain.emit()
        self.focusList.emit(0)

    def _switchFocusUp(self):
        lastRow = self.parent.recentList.count() - 1
        self.focusList.emit(lastRow)

    def _switchFocusDown(self):
        self.focusList.emit(0)


class RecentList(CommonList):
    focusDraft = QtCore.pyqtSignal()

    def __init__(self, parent):
        super().__init__(parent)

    def hideEvent(self, event):
        # Force focus on draft when recent list is empty
        if self.parent.draftList.isVisible:
            self.focusDraft.emit()

    def _deleteItemHook(self):
        if self.currentItem():
            currentText = self.currentItem().text()
            self.discard.emit(currentText)
            if self.parent.view == "match":
                currentRow = self.currentRow()
                self.takeItem(currentRow)

        self.resizeMain.emit()
        if self.count() == 0:
            self.focusDraft.emit()

    def _switchFocusDown(self):
        if self.parent.draftList.isVisible():
            self.focusDraft.emit()
        else:
            self.setCurrentRow(0)

    def _switchFocusUp(self):
        if self.parent.draftList.isVisible():
            self.focusDraft.emit()
        else:
            lastRow = self.count() - 1
            self.setCurrentRow(lastRow)

    def resizeList(self):
        heightTotal = 0
        for row in range(self.count()):
            item = self.item(row)
            width = item.sizeHint().width()
            height = self.sizeHintForRow(row)
            heightTotal += height
            sizeHint = QtCore.QSize(width, height)
            item.setSizeHint(sizeHint)

        if heightTotal:
            self.setMinimumHeight(heightTotal + 2)
            self.setVisible(True)
        else:
            self.setVisible(False)


class FramelessWidget(QtWidgets.QWidget):
    def __init__(self):
        super().__init__()
        self.setAttribute(QtCore.Qt.WA_X11NetWmWindowTypeSplash)
        self.setWindowFlags(QtCore.Qt.FramelessWindowHint | QtCore.Qt.WindowStaysOnTopHint)

    def mouseMoveEvent(self, event):
        # Mouse dragging for frameless windows
        if event.buttons() == QtCore.Qt.LeftButton:
            self.move(event.globalPos() - self.dragPosition)
            event.accept()

    def mousePressEvent(self, event):
        # Mouse dragging for frameless windows
        if event.button() == QtCore.Qt.LeftButton:
            self.dragPosition = event.globalPos() - self.frameGeometry().topLeft()
            event.accept()

    def paintEvent(self, event):
        # Draw a border for FramelessWindowHint
        borderColor = QtGui.QColor("black")
        bgColor = QtGui.QColor(self.palette().color(QtGui.QPalette.Background))
        painter = QtGui.QPainter(self)
        painter.setPen(QtCore.Qt.NoPen)
        painter.setBrush(QtGui.QBrush(borderColor))
        painter.drawRect(0, 0, self.width(), self.height())
        painter.setBrush(QtGui.QBrush(bgColor))
        painter.drawRect(1, 1, self.width() - 2, self.height() - 2)


class Database():
    def __init__(self, parent):
        path = HISTORY_FILE
        self.parent = parent
        self.preferences = parent.preferences
        self.name = "history"
        self.path = path
        self.log = parent.log
        self._legacyJsonConversion()
        if os.path.isfile(path) and os.stat(path).st_size > 0:
            self.load()
        else:
            self.db = {"history": [], "blacklist": []}
            with open(path, "w") as f:
                f.write(json.dumps(self.db, indent=2, sort_keys=False))
            self.log.info(f"Created default file for '{self.name}'")

        self.history = self.db.get("history", [])
        self.blacklist = self.db.get("blacklist", [])

    def _deleteFromHistory(self, cmd):
        for line in list(self.history):
            if line == cmd:
                self.history.remove(cmd)

    def _legacyJsonConversion(self):
        # Temporary function to convert old history plain text format to JSON (Legacy, june 2019)
        jsonFile = HISTORY_FILE
        hstFile = os.path.expanduser(f"~/.config/{ID}/launcher.hst")
        if not os.path.isfile(jsonFile) and os.path.isfile(hstFile):
            hstDb = {"history": []}
            with open(hstFile) as f:
                for line in reversed(f.readlines()):
                    line = line.strip()
                    hstDb["history"].append(line)
            with open(jsonFile, "w") as f:
                f.write(json.dumps(hstDb, indent=2, sort_keys=False))
            os.remove(hstFile)
            self.log.warning("Converted plain text history file to JSON")

    def add(self, cmd):
        self._deleteFromHistory(cmd)
        self.history.insert(0, cmd)

    def discard(self, cmd):
        self.log.info(f"Deleted '{cmd}' from {self.name} database")
        self._deleteFromHistory(cmd)
        if self.preferences.get("parameters", "launcher", "blacklist"):
            if cmd in self.parent.desktopFiles.cmds:
                self.parent.db.blacklist.append(cmd)
                self.log.info(f"Added '{cmd}' to blacklist")
        self.save()

    def load(self):
        with open(self.path, "r") as f:
            self.db = json.load(f)
        self.log.info(f"Loaded {self.name} database")

    def save(self):
        with open(self.path, "w") as f:
            f.write(json.dumps(self.db, indent=2, sort_keys=False))
        self.log.info(f"Saved {self.name} database")


class DesktopFiles():
    def __init__(self, parent):
        super().__init__()
        self.parent = parent
        self.preferences = parent.preferences
        files = self._find()
        self.cmds, self.icons = self._parse(files)

    def _find(self):
        desktopFiles = set()
        userApps = os.path.expanduser("~/.local/share/applications")
        for folder in (userApps, "/usr/share/applications"):
            try:
                for f in os.listdir(folder):
                    ext = os.path.splitext(f)[1].lower()
                    if ext == ".desktop":
                        desktopFiles.add(f"{folder}/{f}")
            except FileNotFoundError:
                pass
        return desktopFiles

    def _parse(self, files):
        commands, icons = [], {}
        for path in files:
            with open(path) as f:
                content = f.read()

            # Extract Icon and Exec fields from desktop files
            cmd, icon = None, None
            for line in content.splitlines():
                if line.startswith("Icon="):
                    icon = line[5:]
                elif line.startswith("Exec="):
                    cmd = line[5:]
                    cmd = self._parseCmd(cmd)

                # Remove blacklisted items
                if self.preferences.get("parameters", "launcher", "blacklist"):
                    if cmd in self.parent.db.blacklist:
                        cmd = None

                if cmd and icon:
                    commands.append(cmd)
                    icons[cmd] = icon
                    break
        return commands, icons

    def _parseCmd(self, cmd):
        # Remove useless prefixes
        for s in ("/usr/bin/", "/bin/"):
            if cmd.startswith(s):
                cmd = cmd.replace(s, "")

        for s in (" %f", " %F", " %u", " %U"):
            if cmd.endswith(s):
                cmd = cmd.replace(s, "")

        # Replace odd path with shortcut when available (ie. /usr/lib/firefox/firefox -> firefox)
        binaries = os.listdir("/usr/bin")
        if cmd.startswith("/"):
            basename = os.path.basename(cmd)
            if basename in binaries:
                return basename
        return cmd


class Main(FramelessWidget):
    def __init__(self, parent):
        super().__init__()
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        self.preferences = parent.preferences
        self.log = parent.logger.new(name="Launcher")
        self.name = "launcher"
        self.view = "recent"

        path = f"{LOCAL_DIR}../ui/icons"
        self.icons = {}
        for icon in os.listdir(f"{path}/{self.name}"):
            iconName = os.path.splitext(icon)[0]
            self.icons[iconName] = QtGui.QIcon(f"{path}/{self.name}/{icon}")

        self.db = Database(self)
        self.desktopFiles = DesktopFiles(self)

        self.slave = QtCore.QProcess(self)
        self.slave.setStandardOutputFile(QtCore.QProcess.nullDevice())
        self.slave.setStandardErrorFile(QtCore.QProcess.nullDevice())
        self.cmdLine = CommandLine(self)
        self.draftList = DraftList(self)
        self.recentList = RecentList(self)

        self.cmdLine.textEdited.connect(self.suggestPattern)
        self.cmdLine.execute.connect(self.execute)
        self.cmdLine.clearRecent.connect(self.clearRecent)
        self.cmdLine.close.connect(self.hide)
        self.cmdLine.focusOut.connect(self.focusOut)
        self.cmdLine.focusList.connect(self.focusList)
        self.cmdLine.focusDraft.connect(self.focusDraft)
        self.cmdLine.hideDraft.connect(self.draftList.hide)
        self.cmdLine.saveDraft.connect(self.saveDraft)
        self.cmdLine.resizeMain.connect(self.resizeMain)
        self.cmdLine.setViewMode.connect(self.setViewMode)

        self.draftList.execute.connect(self.execute)
        self.draftList.focusCmd.connect(self.cmdLine.setFocus)
        self.draftList.focusOut.connect(self.focusOut)
        self.draftList.resizeMain.connect(self.resizeMain)
        self.draftList.updateCmd.connect(self.cmdLine.setText)
        self.draftList.discard.connect(self.db.discard)
        self.draftList.focusList.connect(self.focusList)

        self.recentList.execute.connect(self.execute)
        self.recentList.focusCmd.connect(self.cmdLine.setFocus)
        self.recentList.focusOut.connect(self.focusOut)
        self.recentList.resizeMain.connect(self.resizeMain)
        self.recentList.updateCmd.connect(self.cmdLine.setText)
        self.recentList.discard.connect(self.db.discard)
        self.recentList.focusDraft.connect(self.focusDraft)
        self.recentList.suggestRecent.connect(self.suggestRecent)

        self.spacer = QtWidgets.QSpacerItem(0, 4, QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        self.layout = QtWidgets.QVBoxLayout()
        self.layout.setContentsMargins(15, 15, 15, 15)
        self.layout.setSpacing(0)
        self.layout.addWidget(self.cmdLine)
        self.layout.addSpacerItem(self.spacer)
        self.layout.addWidget(self.draftList)
        self.layout.addWidget(self.recentList)
        self.setLayout(self.layout)
        self.setMinimumWidth(410)
        self.resize(0, 0)

    def _aliasFetch(self):
        alias = {}
        with open(os.path.expanduser("~/.bashrc")) as f:
            for line in f.readlines():
                if line.startswith("alias "):
                    line = line[len("alias "):].strip()
                    line = line.split("=", maxsplit=1)
                    cmd = os.path.expanduser(line[1][1:-1])
                    alias[line[0]] = cmd
        return alias

    def _autoComplete(self, pattern):
        for cmd in self.db.history:
            if self._match(cmd, pattern):
                length = len(cmd) - len(pattern)
                self.cmdLine.setText(cmd)
                self.cmdLine.setSelection(len(pattern), length)
                break

    def _match(self, cmd, pattern):
        if self.preferences.get("parameters", "launcher", "case sensitive"):
            match = cmd.startswith(pattern)
        else:
            match = bool(re.match(pattern, cmd, re.IGNORECASE))
        return match

    def _parse(self, cmd):
        cmd = shlex.split(cmd)
        for i, c in enumerate(cmd):
            cmd[i] = os.path.expanduser(c)

        aliases = self._aliasFetch()
        for alias in aliases:
            if alias == cmd[0]:
                del cmd[0]
                trans = aliases[alias]
                cmd = shlex.split(trans) + cmd
        return cmd

    def _searchPattern(self, pattern):
        maximum = self.preferences.get("parameters", "launcher", "suggestions amount")
        found = set()

        # Look for matching pattern in the history
        for index, cmd in enumerate(self.db.history):
            if self.recentList.count() == maximum:
                break
            if index == 0 and not self.cmdLine.backspace:
                self._autoComplete(pattern)
                found.add(self.cmdLine.text())
            elif self._match(cmd, pattern) and cmd not in found:
                found.add(cmd)
                item = CommonListItem(self, cmd, icon="recent")
                self.recentList.addItem(item)

        # Fill empty spaces with matching commands from desktop files
        if self.preferences.get("parameters", "launcher", "desktop files"):
            for cmd in self.desktopFiles.cmds:
                if self.recentList.count() == maximum:
                    break
                if self._match(cmd, pattern) and cmd not in found:
                    found.add(cmd)
                    item = CommonListItem(self, cmd, icon="desktop")
                    self.recentList.addItem(item)

    def _setErrorPalette(self):
        fg = QtGui.QColor("#ffc7ce")
        bg = QtGui.QColor("#9c0006")
        palette = QtGui.QPalette()
        palette.setColor(QtGui.QPalette.Base, bg)
        palette.setColor(QtGui.QPalette.Text, fg)
        self.cmdLine.setPalette(palette)

    def showEvent(self, event):
        self.cmdLine.reload()
        if self.preferences.get("parameters", "launcher", "startup suggestions"):
            self.suggestRecent()
        self.activateWindow()
        self.cmdLine.setFocus(True)

    def clearRecent(self):
        self.draftList.hide()
        self.recentList.hide()
        self.resizeMain()

    def execute(self):
        raw = self.cmdLine.text().strip()
        if raw:
            cmd = self._parse(raw)
            self.slave.setProgram(cmd[0])
            self.slave.setArguments(cmd[1:])
            if self.slave.startDetached()[0]:
                self.db.add(raw)
                self.db.save()
                self.hide()
            else:
                self._setErrorPalette()
        else:
            self.hide()

    def focusDraft(self):
        if self.draftList.isVisible():
            self.recentList._clearSelection()
            self.draftList.setCurrentRow(-1)
            self.draftList.setFocus(True)
            self.draftList.setCurrentRow(0)
        else:
            if self.recentList.isVisible():
                self.focusList(0)

    def focusOut(self):
        if self.preferences.get("parameters", "launcher", "close on focus out"):
            if not self.cmdLine.hasFocus() and not self.recentList.hasFocus() and not self.draftList.hasFocus():
                self.hide()

    def focusList(self, row):
        self.draftList._clearSelection()
        self.recentList.setFocus(True)
        self.recentList.setCurrentRow(row)

    def resizeMain(self):
        self.recentList.resizeList()
        QtCore.QCoreApplication.processEvents()
        self.resize(0, 0)

    def saveDraft(self, cmd):
        if cmd and self.preferences.get("parameters", "launcher", "save draft"):
            for i in range(self.recentList.count()):
                if self.recentList.item(i).text() == cmd:
                    break
            else:
                self.draftList.item(0).setText(cmd)
                if self.recentList.isVisible():
                    self.draftList.show()

    def setViewMode(self, mode):
        self.view = mode

    def suggestPattern(self, pattern):
        if pattern:
            self.recentList.clear()
            self._searchPattern(pattern)
            self.resizeMain()
        else:
            self.suggestRecent()

    def suggestRecent(self):
        self.view = "recent"
        self.recentList.clear()
        maximum = self.preferences.get("parameters", "launcher", "suggestions amount")
        cmd = self.cmdLine.text()
        for lastest in self.db.history:
            if self.recentList.count() == maximum:
                break
            if lastest != cmd:
                item = CommonListItem(self, cmd=lastest, icon="recent")
                self.recentList.addItem(item)

        draftText = self.draftList.item(0).text()
        for i in range(self.recentList.count()):
            if self.recentList.item(i).text() == draftText:
                self.draftList.hide()
                break
        else:
            if draftText and not cmd:
                self.draftList.show()
        self.resizeMain()
