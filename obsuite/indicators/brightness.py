#!/usr/bin/python3
import datetime
import os
from PyQt5 import QtCore, QtDBus

try:
    from ..backend.common import IndicatorIcon
except (ImportError, ValueError):
    from backend.common import IndicatorIcon

LOCAL_DIR = os.path.dirname(os.path.realpath(__file__)) + '/'


class KeyboardBacklight():
    def __init__(self, parent):
        super().__init__()
        self.log = parent.log

        service = "org.freedesktop.UPower"
        if QtDBus.QDBusConnection.systemBus().interface().isServiceRegistered(service).value():
            path = "/org/freedesktop/UPower/KbdBacklight"
            interface = "org.freedesktop.UPower.KbdBacklight"
            bus = QtDBus.QDBusConnection.systemBus()
            self.interface = QtDBus.QDBusInterface(service, path, interface, bus)
            self.maximum = self.interface.call("GetMaxBrightness").arguments()[0]
        else:
            self.log.warning("Could not connect to UPower bus")

    def on(self, level=0):
        if hasattr(self, "interface"):
            if not level:
                level = self.maximum
            self.interface.call("SetBrightness", level)

    def off(self):
        if hasattr(self, "interface"):
            self.interface.call("SetBrightness", 0)

    def toggle(self):
        if hasattr(self, "interface"):
            if self.interface.call("GetBrightness").arguments()[0]:
                self.off()
            else:
                self.on()


class Status(QtCore.QObject):
    popup = QtCore.pyqtSignal(str)
    fetch = QtCore.pyqtSignal()

    def __init__(self, parent):
        super().__init__()
        self.ready = False
        self.slave = parent.slave
        self.preferences = parent.preferences
        self.icon = "default"
        self.lastBrightness = -1
        self.lastLevel = 0
        self.level = 0
        self.dim = False

    def _dim(self):
        start = self.preferences.get("parameters", "brightness", "dim start")
        stop = self.preferences.get("parameters", "brightness", "dim end")
        start = datetime.datetime.strptime(start, '%H:%M:%S').time()
        stop = datetime.datetime.strptime(stop, '%H:%M:%S').time()
        now = datetime.datetime.now().time()

        dim = now > start and now < stop
        if not dim == self.dim:
            if dim:
                low = self.preferences.get("parameters", "brightness", "dim level")
                self.slave.startDetached(f"xbacklight -set {low}")
                self.lastLevel = self.level
            else:
                if self.level < self.lastLevel:
                    self.slave.startDetached(f"xbacklight -set {self.lastLevel}")
        self.dim = dim

    def _parse(self, output):
        self.level = int(output.split(".")[0])

    def update(self, output):
        self._parse(output)
        if not self.lastBrightness == self.level:
            if self.lastBrightness > -1:
                self.popup.emit(f"{self.level}%")

        if self.preferences.get("parameters", "brightness", "dim"):
            self._dim()

        self.lastBrightness = self.level
        self.ready = True
        self.fetch.emit()


class Main(IndicatorIcon):
    def _initHook(self):
        self.status = Status(self)
        self.slave.ready.connect(self._update)
        self.status.popup.connect(self.popup.emit)
        self.parent.initIndicator(self, "brightness")
        self.kbdBacklight = KeyboardBacklight(self)

    def _cutoffDecrease(self, command):
        minimum = self.preferences.get("parameters", "brightness", "minimum") + 1
        length = len("xbacklight -dec ")
        try:
            self.slave.fetch("xbacklight -get")
            self.slave.waitForFinished()
            dec = int(command[length:])
            if (self.status.level - dec) <= minimum:
                command = f"xbacklight -set {minimum}"
        except ValueError:
            pass
        return command

    def _fetch(self):
        self.slave.fetch("xbacklight -get")

    def _update(self, output):
        self.status.update(output)
        self.setToolTip(f"Brightness: {self.status.level}%")

    def action(self, action, command=""):
        if action == "exec":
            if command.startswith("xbacklight -dec"):
                command = self._cutoffDecrease(command)
            self.execute.emit(command)

        elif action == "toggle brightness level":
            if self.status.level >= self.preferences.get("parameters", "brightness", "threshold"):
                self.slave.startDetached(self.preferences.get("parameters", "brightness", "low command"))
            else:
                self.slave.startDetached(self.preferences.get("parameters", "brightness", "high command"))

        elif action == "toggle keyboard backlight":
            self.kbdBacklight.toggle()

        elif action == "start keyboard backlight":
            try:
                level = int(command)
            except ValueError:
                level = 0
            self.kbdBacklight.on(level)

        elif action == "stop keyboard backlight":
            self.kbdBacklight.off()

        else:
            self.log.error(f"Invalid action '{action}'")
