#!/usr/bin/python3
import os
from PyQt5 import QtCore, QtDBus

LOCAL_DIR = os.path.dirname(os.path.realpath(__file__)) + '/'


class Main(QtCore.QObject):
    execute = QtCore.pyqtSignal(str)

    def __init__(self, parent):
        super().__init__()
        self.name = "desktop bus"
        self.preferences = parent.preferences
        self.log = parent.logger.new("Desktop bus")
        self.bus = QtDBus.QDBusConnection.systemBus()

        # Connect signal and slots for the login manager
        service = "org.freedesktop.login1"
        if self.bus.interface().isServiceRegistered(service).value():
            path = "/org/freedesktop/login1"
            interface = "org.freedesktop.login1.Manager"
            self.bus.connect(service, path, interface, "PrepareForShutdown", self._prepareForShutdown)
            self.bus.connect(service, path, interface, "PrepareForSleep", "b", self._prepareForSleep)

        # Connect signal and slots for the AC status
        service = "org.freedesktop.UPower"
        if self.bus.interface().isServiceRegistered(service).value():
            devices = ("AC", "AC0", "ACAD", "ADP1")
            interface = "org.freedesktop.DBus.Properties"
            for d in devices:
                # QtBug: return true on failed connection
                # Hence, bind the ac slot on all possible paths
                path = f"/org/freedesktop/UPower/devices/line_power_{d}"
                self.bus.connect(service, path, interface, "PropertiesChanged", self._acPropertiesChanged)
        else:
            self.log.warning("Could not connect to UPower bus")

    def _getParameters(self, trigger):
        enabled, action = self.preferences.get("actions", "desktop bus", trigger)
        command = self.preferences.get("exec", "desktop bus", trigger)
        return enabled, action, command

    @QtCore.pyqtSlot()
    def _prepareForShutdown(self):
        enabled, action, command = self._getParameters("prepare for shutdown")
        if enabled and action == "Exec":
            self.log.info("Prepare for shutdown")
            self.execute.emit(command)

    @QtCore.pyqtSlot(bool)
    def _prepareForSleep(self, state):
        if state:
            enabled, action, command = self._getParameters("prepare for sleep")
            state = "Prepare for"
        else:
            enabled, action, command = self._getParameters("resume from sleep")
            state = "Resume from"

        if enabled and action == "Exec":
            self.execute.emit(command)
            self.log.info(f"{state} sleep")

    @QtCore.pyqtSlot()
    def _acPropertiesChanged(self):
        # Fetch new AC status
        service = "org.freedesktop.UPower"
        path = "/org/freedesktop/UPower/devices/line_power_AC"
        interface = "org.freedesktop.DBus.Properties"
        bus = QtDBus.QDBusConnection.systemBus()
        connection = QtDBus.QDBusInterface(service, path, interface, bus)
        status = connection.call("Get", "org.freedesktop.UPower.Device", "Online").arguments()[0]

        # Fetch and run custom action, if any
        if status:
            enabled, action, command = self._getParameters("power connected")
            state = "connected"
        else:
            enabled, action, command = self._getParameters("power disconnected")
            state = "disconnected"

        if enabled and action == "Exec":
            self.log.info(f"Power supply {state}")
            self.execute.emit(command)
