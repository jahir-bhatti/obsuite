#!/usr/bin/python3
import os
import re
from PyQt5 import QtWidgets, QtCore, QtDBus

try:
    from ..backend.common import IndicatorIcon
except (ImportError, ValueError):
    from backend.common import IndicatorIcon

LOCAL_DIR = os.path.dirname(os.path.realpath(__file__)) + '/'
TIMEOUT = 6000  # ##


class Status(QtCore.QObject):
    popup = QtCore.pyqtSignal(str)
    fetch = QtCore.pyqtSignal()

    def __init__(self, parent):
        super().__init__()
        self.slave = parent.slave
        self.ready = False
        self.lastMute = False
        self.lastLevel = -1

        # Match string such as "Front Left: Playback 19661 [30%] [on]"
        # Capture the level (30) and the state (on)
        self.pattern = re.compile(r"Playback\s\d+\s\[(\d{1,3})%\]\s\[(off|on)\]")

    def _parse(self, stdout):
        match = re.findall(self.pattern, stdout)
        if match:
            try:
                # Put the numer in the first tuple, the on/off state in the second
                state = zip(match[0], match[1])
                state = list(state)
                mute = "on" not in state[1]

                # Calculate the average between left & right channel
                right, left = int(state[0][0]), int(state[0][1])
                level = int((right + left) / 2)
            except IndexError:
                # Only one channel available
                state = match[0]
                mute = "on" not in state
                level = int(state[0][0])
            return level, mute
        return 0, False

    def update(self, output):
        level, mute = self._parse(output)

        if level == 0 or mute:
            self.icon = "mute"
        elif level > 66:
            self.icon = "high"
        elif level > 33:
            self.icon = "medium"
        elif level > 0:
            self.icon = "low"

        if not self.lastLevel == level:
            if self.lastLevel > -1:
                self.popup.emit(f"{level}%")
        elif not self.lastMute == mute:
            msg = "Mute enabled" if mute else "Mute disabled"
            msg = f"{msg}\n{level}%"
            self.popup.emit(msg)

        self.lastLevel = level
        self.lastMute = mute
        self.ready = True
        self.fetch.emit()


class Bluez(QtCore.QObject):
    popupQueued = QtCore.pyqtSignal(str)
    togglingDone = QtCore.pyqtSignal()

    def __init__(self, parent):
        super().__init__()
        self.parent = parent
        self.preferences = parent.preferences
        self.log = parent.parent.logger.new("Bluetooth")
        self.lastStatus = False
        self.setInterfaces()
        self.statusTimer = QtCore.QTimer(interval=1000)
        self.statusTimer.timeout.connect(self._statusQuery)
        self.statusTimer.start()

    def setInterfaces(self):
        self.reply = []
        self.devices = {}
        self.properties = {}
        for device in self.preferences.get("parameters", "bluetooth"):
            self._addInterface(device)

    def _addInterface(self, path):
        service = "org.bluez"
        bus = QtDBus.QDBusConnection.systemBus()
        self.properties[path] = QtDBus.QDBusInterface(service, path, "org.freedesktop.DBus.Properties", bus)
        self.devices[path] = QtDBus.QDBusInterface(service, path, "org.bluez.Device1", bus)
        self.devices[path].setTimeout(TIMEOUT)

    def _connectAttempt(self):
        # Connect to the first available bluetooth device
        for path in self.devices:
            name = self.preferences.get("parameters", "bluetooth", path)
            self.popupQueued.emit(f"Connecting to...\n{name}")
            reply = self.devices[path].call("Connect")
            error = reply.arguments()[0]
            if error:
                self.log.info(f"Connection failed: {error}")
            else:
                self.log.info("Connection successful")
                break
        else:
            self.popupQueued.emit("Connections failed")

    def _isConnected(self):
        for path in self.properties:
            reply = self.properties[path].call("Get", "org.bluez.MediaControl1", "Connected")
            connected = reply.arguments()[0]
            if connected:
                return True
        return False

    def _statusQuery(self):
        for path in self.properties:
            self.properties[path].callWithCallback("Get", ["org.bluez.MediaControl1", "Connected"], self.slotStatus)

    def _statusCompare(self):
        self.status = sum(self.reply)
        if not self.lastStatus == self.status:
            if self.status > self.lastStatus:
                self.popupQueued.emit("Bluetooth connected")
            else:
                self.popupQueued.emit("Bluetooth disconnected")
            self.lastStatus = self.status
        self.reply = []

    def connect(self, path):
        name = self.preferences.get("parameters", "bluetooth", path)
        self.popupQueued.emit(f"Connecting to...\n{name}")
        self.devices[path].asyncCall("Connect")

    def disconnect(self, path):
        self.devices[path].asyncCall("Disconnect")

    @QtCore.pyqtSlot()
    def disconnectAll(self):
        for path in self.properties:
            name = self.preferences.get("parameters", "bluetooth", path)
            self.devices[path].asyncCall("Disconnect")
            self.log.info(f"Called 'Disconnect' on '{name}'")

    @QtCore.pyqtSlot(QtDBus.QDBusMessage)
    def slotStatus(self, reply):
        self.reply.append(reply.arguments()[0])
        if len(self.reply) == len(self.devices):
            self._statusCompare()

    @QtCore.pyqtSlot()
    def toggleConnection(self):
        if self._isConnected():
            self.disconnectAll()
        else:
            self._connectAttempt()
        self.togglingDone.emit()


class Main(IndicatorIcon):
    def _initHook(self):
        self.status = Status(self)
        self.status.popup.connect(self.popup.emit)
        self.slave.ready.connect(self._update)

        self.bluezToggling = False
        self.bluez = Bluez(self)
        self.bluezThread = QtCore.QThread()
        self.bluez.moveToThread(self.bluezThread)
        self.bluez.togglingDone.connect(self._bluezTogglingDone)
        self.bluez.popupQueued.connect(self.popupQueued.emit)
        self.bluezThread.start()

        self.parent.initIndicator(self, "volume")
        if self.preferences.get("parameters", "volume", "enable"):
            self.initMenu()

    def _bluezTogglingDone(self):
        self.bluezToggling = False

    def _fetch(self):
        self.slave.fetch("amixer scontents")

    def _update(self, output):
        self.status.update(output)
        self.setToolTip(f"Volume: {self.status.lastLevel}%")
        self.setIcon(self.icons[self.status.icon])

    def action(self, action, command=""):
        if action == "exec":
            self.execute.emit(command)

        elif action == "toggle sound level":
            if self.status.lastLevel >= self.preferences.get("parameters", "volume", "threshold"):
                self.slave.start(self.preferences.get("parameters", "volume", "low command"))
            else:
                self.slave.start(self.preferences.get("parameters", "volume", "high command"))
            self.slave.waitForFinished()

        elif action == "toggle bluetooth":
            if not self.bluezToggling:
                self.bluezToggling = True
                QtCore.QMetaObject.invokeMethod(self.bluez, "toggleConnection", QtCore.Qt.QueuedConnection)

        elif action == "disconnect bluetooth":
            if not self.bluezToggling:
                QtCore.QMetaObject.invokeMethod(self.bluez, "disconnectAll", QtCore.Qt.QueuedConnection)

        else:
            self.log.error(f"Invalid action '{action}'")

    def initMenu(self):
        self.menu = QtWidgets.QMenu()
        for device in self.preferences.get("parameters", "bluetooth"):
            name = self.preferences.get("parameters", "bluetooth", device)
            self.menu.addAction(self.icons["connect"], f"Connect {name}", lambda device=device: self.bluez.connect(device))
        self.menu.addSeparator()
        self.menu.addAction(self.icons["disconnect"], "Disconnect all", self.bluez.disconnectAll)
        self.setContextMenu(self.menu)
